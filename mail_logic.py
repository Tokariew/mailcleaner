import imaplib
import json

with open('additional.json') as file:
    mail_exception = json.loads(file.read())


def parse_mailbox(data):
    flags, b, c = data.partition(')')
    flags += ')'
    seperator, b, name = c.rpartition(' ')
    return (flags.lower(), seperator.replace(' ', ''), name)


def imap_adress(domain, dict_list):
    for el in dict_list:
        if domain == el['domain']:
            return el['imap']
    else:
        return 'imap.' + domain


def logowanie(login, password):
    log = ['', '']
    imp = login[login.find('@') + 1:]
    imp = imap_adress(imp, mail_exception)
    try:
        mail = imaplib.IMAP4_SSL(imp)
    except imaplib.socket.gaierror:
        log[0] = 0
        log[1] += 'Unkown server, log in failed\n'
        return log, None

    try:
        mail.login(login, password)
        log[0] = 1
        log[1] += 'Log in with: {}\n'.format(login)
        return log, mail
    except imaplib.IMAP4.error as err:
        log[0] = 0
        log[1] += '{}\n'.format(err.args[0].decode())
        return log, mail


def clean_up(mail, log=''):
    stat, res = mail.list()
    deleted = 0
    for folder in res:
        try:
            flags, sep, name = parse_mailbox(folder.decode('utf-8'))
            if flags.find('Noselect') == -1:
                result, info = mail.select(name)
                if result != 'NO' and info[0] != b'0':
                    deleted += int(info[0].decode())
                    mail.store('1:*', '+FLAGS', '\\Deleted')
                    mail.expunge()
                    log += 'Deleted {} from {}\n'.format(
                        info[0].decode(), name)
                elif result != 'NO' and info[0] == b'0':
                    log += '{} already empty\n'.format(name)
                else:
                    log += '{}\n'.format(info[0].decode())
        except:
            log += 'Unkown Error\n'
    if deleted != 0:
        log = clean_up(mail, log)
    return log


def unspam(mail):
    log = ''
    stat, res = mail.list()
    for folder in res:
        flags, sep, name = parse_mailbox(folder.decode())
        if flags.find('junk') != -1 or flags.find(
                'spam') != -1 or name.lower().find('spam') != -1:
            result, info = mail.select(name)
            if result != 'NO' and info[0] != b'0':
                res, ini = mail.xatom('MOVE', '1:*', 'inbox')
                if res == 'OK':
                    log += 'Moved {} to inbox\n'.format(info[0].decode())
                else:
                    log += 'Error: {}\n'.format(ini[0].decode())
            else:
                log += 'No mail in spam\n'
            break
    else:
        log += 'No spam folder found\n'
    return log


def logout(mail):
    mail.select('Inbox')
    mail.close()
    mail.logout()


if __name__ == '__main__':
    ret, mail = logowanie('dicrotis5m@mail.ru', 'ti5t0tu2p')
    print(ret)
