import threading

import mail_logic as ml
from kivy.app import App
from kivy.core.clipboard import Clipboard
from kivy.uix.behaviors import FocusBehavior
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.recycleboxlayout import RecycleBoxLayout
from kivy.uix.recycleview import RecycleView
from kivy.uix.recycleview.layout import LayoutSelectionBehavior
from kivy.uix.recycleview.views import RecycleDataViewBehavior
from kivy.uix.widget import Widget


class SelectableRecycleBoxLayout(FocusBehavior, LayoutSelectionBehavior,
                                 RecycleBoxLayout):
    ''' Adds selection and focus behaviour to the view. '''


class Row(RecycleDataViewBehavior, BoxLayout):
    ''' Add selection support to the Label '''
    index = 0

    def refresh_view_attrs(self, rv, index, data):
        ''' Catch and handle the view changes '''
        self.index = data['number']
        return super(Row, self).refresh_view_attrs(rv, index, data)


class RV(RecycleView):
    def __init__(self, **kwargs):
        super(RV, self).__init__(**kwargs)
        self.data = []


class MailWidget(Widget):
    def __init__(self):
        super(MailWidget, self).__init__()
        self.mail_list = []
        self.number = 1

    def imp(self):
        lista = Clipboard.paste()
        itstr = iter(lista.splitlines())
        for line in itstr:
            if line[0:2] == 'L:':
                nxt_line = next(itstr)
                if nxt_line[0:2] == 'P:':
                    if [
                            item for item in self.mail_list
                            if item['login'] == line[3:]
                    ] == []:
                        self.mail_list.append({
                            'number': self.number,
                            'login': line[3:],
                            'password': nxt_line[3:]
                        })
                        self.number += 1
        self.ids.list.data = self.mail_list

    def delete_me(self):
        self.mail_list = []
        self.number = 1
        self.ids.list.data = self.mail_list
        self.ids.fake_one.text = ''

    def run_me(self, text):
        if text.find('Clean') != -1:
            thread = threading.Thread(target=self.help_clean)
            thread.start()
        elif text.find('Move Spam') != -1:
            thread = threading.Thread(target=self.help_unspam)
            thread.start()

    def help_clean(self):
        lg = len(self.ids.list.data)
        self.ids.pb.max = lg
        self.ids.pb.value = 0
        for i, mail in enumerate(self.ids.list.data):
            self.ids.fake_one.text += 'Account {}/{}:\n'.format(i + 1, lg)
            login, password = mail['login'], mail['password']
            log, mail_acc = ml.logowanie(login, password)
            self.ids.fake_one.text += log[1]
            if log[0]:
                log = ml.clean_up(mail_acc)
                self.ids.fake_one.text += log
                ml.logout(mail_acc)
            self.ids.fake_one.text += '\n'
            self.ids.pb.value += 1

    def help_unspam(self):
        lg = len(self.ids.list.data)
        self.ids.pb.max = lg
        self.ids.pb.value = 0
        for i, mail in enumerate(self.ids.list.data):
            self.ids.fake_one.text += 'Account {}/{}:\n'.format(i + 1, lg)
            login, password = mail['login'], mail['password']
            log, mail_acc = ml.logowanie(login, password)
            self.ids.fake_one.text += log[1]
            if log[0]:
                log = ml.unspam(mail_acc)
                self.ids.fake_one.text += log
                ml.logout(mail_acc)
            self.ids.fake_one.text += '\n'
            self.ids.pb.value += 1


class MailCleanerApp(App):
    use_kivy_settings = False

    def build(self):
        self.icon = 'mail.png'
        self.title = 'MailCleaner by Acilia'
        return MailWidget()

    def open_settings(self, *largs):
        pass


if __name__ == '__main__':
    MailCleanerApp().run()
